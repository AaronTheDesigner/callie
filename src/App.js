import React from 'react';
import './App.css';

const App = () => {
  return (
    <div>
      <header className="hero">
        <div className="container spacing">
          <h1 className="primary-title">Home Decor by Callie</h1>
          <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim.</p>
          <a href="#" className="btn">See What We Have</a>
        </div>
      </header>

      <main>
        <section className="featured">
          <div className="container">
              <h2 className="section-title">Featured Services</h2>
              <div className="split">
                
                <a href="#" className="featured__item">
                  <img src={require('./assets/img_3.jpg')} alt="" className="featured__img"/>
                  <p className="featured__details"> <span className="service">$99</span>Consultation</p>
                </a>
                
                <a href="#" className="featured__item">
                  <img src={require('./assets/img_4.jpg')} alt="" className="featured__img"/>
                  <p className="featured__details"> <span className="service">$350</span>Design</p>
                </a>
                
                <a href="#" className="featured__item">
                  <img src={require('./assets/img_5.jpg')} alt="" className="featured__img"/>
                  <p className="featured__details"> <span className="service">$Custom</span>Furnish</p>
                </a>
              </div>
          </div>
          </section>

        <section className="products">
          <div className="container">
            <h2 className="section-title">Testimonials</h2>
            
            <article className="product spacing cust-blue">
              <div className="img-group">
                <img src={require('./assets/cust_1.jpg')} alt="" className="cust__image"/>
                <img src={require('./assets/img_7.jpg')} alt="" className="product__image"/>
              </div>              
              <h3 className="product__title">Customer Name</h3>
              <p className="product__description">Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
              <a href="#" className="btn">Hear More</a>
            </article>

            <article className="product spacing cust-brown">
              <div className="img-group">
                <img src={require('./assets/cust_2.jpg')} alt="" className="cust__image"/>
                <img src={require('./assets/img_8.jpg')} alt="" className="product__image"/>
              </div>              
              <h3 className="product__title">Customer Name</h3>
              <p className="product__description">Cras ultricies ligula sed magna dictum porta.</p>
              <a href="#" className="btn">Hear More</a>
            </article>

            <article className="product spacing cust-white">
              <div className="img-group">
                <img src={require('./assets/cust_3.jpg')} alt="" className="cust__image"/>
                <img src={require('./assets/img_6.jpg')} alt="" className="product__image"/>
              </div>
              <h3 className="product__title">Customer Name</h3>
              <p className="product__description">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
              <a href="#" className="btn">Hear More</a>
            </article> 
          </div>
        </section>

      </main>
    </div>
  )
}
export default App;
